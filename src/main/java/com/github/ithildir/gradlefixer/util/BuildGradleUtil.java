/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrea Di Giorgi
 */
public class BuildGradleUtil {

	@SuppressWarnings("unchecked")
	public static List<String> getDependencies(Path buildGradlePath)
		throws IOException {

		if (Files.notExists(buildGradlePath)) {
			return Collections.emptyList();
		}

		String buildGradle = FileUtil.read(buildGradlePath);

		Object[] tuple = getDependencies(buildGradle);

		if (tuple == null) {
			return Collections.emptyList();
		}

		return (List<String>)tuple[0];
	}

	public static Object[] getDependencies(String buildGradle) {
		Matcher matcher = _DEPENDENCIES_PATTERN.matcher(buildGradle);

		if (!matcher.find()) {
			return null;
		}

		List<String> dependencies = StringUtil.split(
			matcher.group(1), "\\r?\\n");

		return new Object[] {dependencies, matcher};
	}

	public static DependencyBag getDependencyBag(String dependency) {
		Matcher matcher = _moduleDependencyPattern.matcher(dependency);

		if (matcher.find()) {
			return _getModuleDependencyBag(dependency, matcher);
		}

		matcher = _projectDependencyPattern.matcher(dependency);

		if (matcher.find()) {
			return _getProjectDependencyBag(dependency, matcher);
		}

		throw new IllegalArgumentException("Unable to parse " + dependency);
	}

	private static ModuleDependencyBag _getModuleDependencyBag(
		String dependency, Matcher matcher) {

		String dependencyConfiguration = matcher.group(1);
		String dependencyGroup = matcher.group(2);
		String dependencyName = matcher.group(3);

		String transitiveString = matcher.group(4);

		boolean transitive = true;

		if (Validator.isNotNull(transitiveString)) {
			transitive = Boolean.parseBoolean(transitiveString);
		}

		String version = matcher.group(5);

		return new ModuleDependencyBag(
			dependencyConfiguration, dependency, dependencyGroup,
			dependencyName, transitive, version);
	}

	private static DependencyBag _getProjectDependencyBag(
		String dependency, Matcher matcher) {

		String configuration = matcher.group(1);
		String project = matcher.group(2);

		return new ProjectDependencyBag(configuration, dependency, project);
	}

	private static final Pattern _DEPENDENCIES_PATTERN = Pattern.compile(
		"^dependencies \\{(.+?)\\}", Pattern.DOTALL | Pattern.MULTILINE);

	private static final Pattern _moduleDependencyPattern = Pattern.compile(
		"(.+) group: \"(.+)\", name: \"(.+)\", " +
			"(?:transitive: (true|false), )?version: \"?([^\"]+)\"?");
	private static final Pattern _projectDependencyPattern = Pattern.compile(
		"(.+) project\\(\"(.+)\"\\)");

}
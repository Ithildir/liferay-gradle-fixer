/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

import java.util.Properties;

/**
 * @author Andrea Di Giorgi
 */
public class PropertiesUtil {

	public static String get(Properties properties, String key) {
		String value = properties.getProperty(key);

		if (value != null) {
			return value;
		}

		key = key.toLowerCase();

		for (String curKey : properties.stringPropertyNames()) {
			if (key.equals(curKey.toLowerCase())) {
				return properties.getProperty(curKey);
			}
		}

		return null;
	}

}
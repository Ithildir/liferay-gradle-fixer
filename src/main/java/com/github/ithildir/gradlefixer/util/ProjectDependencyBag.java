/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

import java.io.File;

import java.nio.file.Path;

/**
 * @author Andrea Di Giorgi
 */
public class ProjectDependencyBag extends BaseDependencyBag {

	public ProjectDependencyBag(
		String configuration, String dependency, String project) {

		super(configuration, dependency);

		_project = project;
	}

	@Override
	public String getKey() {
		return _project;
	}

	public String getProject() {
		return _project;
	}

	public Path getProjectDirPath(Path rootDirPath) {
		String relativePath = _project;

		if (relativePath.charAt(0) == ':') {
			relativePath = relativePath.substring(1);
		}

		relativePath = relativePath.replace(':', File.separatorChar);

		return rootDirPath.resolve(relativePath);
	}

	private final String _project;

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

/**
 * @author Andrea Di Giorgi
 */
public abstract class BaseDependencyBag implements DependencyBag {

	public BaseDependencyBag(String configuration, String dependency) {
		_configuration = configuration;
		_dependency = dependency;
	}

	@Override
	public String getConfiguration() {
		return _configuration;
	}

	@Override
	public String getDifferences(DependencyBag other) {
		String differences = "";

		if (!_configuration.equals(other.getConfiguration())) {
			differences = getDifferenceMessage(
				"configuration", _configuration, other.getConfiguration());
		}

		return differences;
	}

	@Override
	public abstract String getKey();

	@Override
	public String toString() {
		return _dependency;
	}

	protected String getDifferenceMessage(
		String property, String value, String otherValue) {

		return String.format(" %s (%s -> %s)", property, value, otherValue);
	}

	private final String _configuration;
	private final String _dependency;

}
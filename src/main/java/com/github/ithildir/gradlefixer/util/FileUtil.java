/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrea Di Giorgi
 */
public class FileUtil {

	public static final DirectoryStream.Filter<Path> directoriesOnlyFilter =
		new DirectoryStream.Filter<Path>() {

			@Override
			public boolean accept(Path path) throws IOException {
				if (Files.isDirectory(path)) {
					return true;
				}

				return false;
			}

		};

	public static Path changeParentPath(
		Path path, Path oldParentPath, Path newParentPath) {

		Path relativePath = oldParentPath.relativize(path);

		return newParentPath.resolve(relativePath);
	}

	public static boolean contains(Path moduleDirPath, String... patterns)
		throws IOException {

		if (Files.notExists(moduleDirPath)) {
			return false;
		}

		final AtomicBoolean found = new AtomicBoolean();

		final PathMatcher[] pathMatchers = _getPathMatchers(
			moduleDirPath, patterns);

		Files.walkFileTree(
			moduleDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(
						Path path, BasicFileAttributes basicFileAttributes)
					throws IOException {

					for (PathMatcher pathMatcher : pathMatchers) {
						if (pathMatcher.matches(path)) {
							found.set(true);

							return FileVisitResult.TERMINATE;
						}
					}

					return FileVisitResult.CONTINUE;
				}

			});

		return found.get();
	}

	public static boolean containsString(
			Path moduleDirPath, String pattern, final String... searchStrings)
		throws IOException {

		if (getPathWithString(moduleDirPath, pattern, searchStrings) != null) {
			return true;
		}

		return false;
	}

	public static String getExtension(Path path) {
		path = path.getFileName();

		String fileName = path.toString();

		int pos = fileName.lastIndexOf('.');

		if (pos <= 0) {
			return "";
		}

		String extension = fileName.substring(pos + 1, fileName.length());

		return extension.toLowerCase();
	}

	public static Set<Path> getFiles(Path dirPath) throws IOException {
		Set<Path> paths = new HashSet<>();

		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(
				dirPath, _filesOnlyFilter)) {

			for (Path path : directoryStream) {
				paths.add(path);
			}
		}

		return paths;
	}

	public static Path getPathWithString(
			Path moduleDirPath, String pattern, final String... searchStrings)
		throws IOException {

		final AtomicReference<Path> pathWithString = new AtomicReference<>(
			null);

		final PathMatcher pathMatcher = _getPathMatcher(moduleDirPath, pattern);

		Files.walkFileTree(
			moduleDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(
						Path path, BasicFileAttributes basicFileAttributes)
					throws IOException {

					if (pathMatcher.matches(path)) {
						String content = read(path);

						for (String searchString : searchStrings) {
							if (content.contains(searchString)) {
								pathWithString.set(path);

								return FileVisitResult.TERMINATE;
							}
						}
					}

					return FileVisitResult.CONTINUE;
				}

			});

		return pathWithString.get();
	}

	public static String getRelativeFileName(Path path, Path rootDirPath) {
		Path relativePath = rootDirPath.relativize(path);

		String relativeFileName = relativePath.toString();

		return relativeFileName.replace('\\', '/');
	}

	public static boolean hasFiles(Path dirPath) throws IOException {
		if (Files.notExists(dirPath)) {
			return false;
		}

		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(
				dirPath, _filesOnlyFilter)) {

			Iterator<Path> iterator = directoryStream.iterator();

			if (iterator.hasNext()) {
				return true;
			}
		}

		return false;
	}

	public static Path move(
			Path path, Path sourceRootDirPath, Path destinationRootDirPath)
		throws IOException {

		Path destinationPath = changeParentPath(
			path, sourceRootDirPath, destinationRootDirPath);

		Files.createDirectories(destinationPath.getParent());

		Files.move(path, destinationPath);

		System.out.println("Moved " + path + " -> " + destinationPath);

		return destinationPath;
	}

	public static String read(ClassLoader classLoader, String location)
		throws IOException {

		try (InputStream is = classLoader.getResourceAsStream(location)) {
			return read(is);
		}
	}

	public static String read(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();

		try (BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(is))) {

			String line = null;

			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
		}

		return sb.toString().trim();
	}

	public static String read(Path path) throws IOException {
		return new String(Files.readAllBytes(path));
	}

	public static Properties readProperties(Path path) throws IOException {
		try (InputStream inputStream = Files.newInputStream(path)) {
			Properties properties = new Properties();

			properties.load(inputStream);

			return properties;
		}
	}

	public static void replace(
			Path path, Iterable<ReplacementBag> replacementBags)
		throws IOException {

		String content = read(path);

		String newContent = content;

		for (ReplacementBag replacementBag : replacementBags) {
			Matcher matcher = replacementBag.pattern.matcher(newContent);

			newContent = matcher.replaceAll(replacementBag.replacement);
		}

		if (!content.equals(newContent)) {
			write(path, newContent);
		}
	}

	public static void replace(
			Path moduleDirPath, final Iterable<ReplacementBag> replacementBags,
			String... patterns)
		throws IOException {

		final PathMatcher[] pathMatchers = _getPathMatchers(
			moduleDirPath, patterns);

		Files.walkFileTree(
			moduleDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(
						Path path, BasicFileAttributes basicFileAttributes)
					throws IOException {

					for (PathMatcher pathMatcher : pathMatchers) {
						if (pathMatcher.matches(path)) {
							replace(path, replacementBags);

							break;
						}
					}

					return FileVisitResult.CONTINUE;
				}

			});
	}

	public static boolean startsWith(Path path, Path... startPaths) {
		for (Path startPath : startPaths) {
			if (path.startsWith(startPath)) {
				return true;
			}
		}

		return false;
	}

	public static void write(Path path, Properties properties)
		throws IOException {

		try (BufferedWriter bufferedWriter = Files.newBufferedWriter(
				path, StandardCharsets.UTF_8)) {

			int i = 0;

			for (String key : properties.stringPropertyNames()) {
				if (i > 0) {
					bufferedWriter.newLine();
				}

				bufferedWriter.write(key);
				bufferedWriter.write(": ");
				bufferedWriter.write(properties.getProperty(key));

				i++;
			}
		}
	}

	public static void write(Path path, String s) throws IOException {
		Files.write(path, s.getBytes());

		System.out.println("Written " + path);
	}

	public static class ReplacementBag {

		public ReplacementBag(String regex, String replacement) {
			pattern = Pattern.compile(regex);
			this.replacement = replacement;
		}

		@Override
		public String toString() {
			return pattern + " -> " + replacement;
		}

		public final Pattern pattern;
		public final String replacement;

	}

	private static PathMatcher _getPathMatcher(
		Path moduleDirPath, String pattern) {

		FileSystem fileSystem = FileSystems.getDefault();

		moduleDirPath = moduleDirPath.toAbsolutePath();

		String patternPrefix = moduleDirPath.toString();

		patternPrefix = patternPrefix.replace('\\', '/') + "/";

		return fileSystem.getPathMatcher("glob:" + patternPrefix + pattern);
	}

	private static PathMatcher[] _getPathMatchers(
		Path moduleDirPath, String[] patterns) {

		PathMatcher[] pathMatchers = new PathMatcher[patterns.length];

		for (int i = 0; i < patterns.length; i++) {
			pathMatchers[i] = _getPathMatcher(moduleDirPath, patterns[i]);
		}

		return pathMatchers;
	}

	private static final DirectoryStream.Filter<Path> _filesOnlyFilter =
		new DirectoryStream.Filter<Path>() {

			@Override
			public boolean accept(Path path) throws IOException {
				if (Files.isRegularFile(path)) {
					return true;
				}

				return false;
			}

		};

}
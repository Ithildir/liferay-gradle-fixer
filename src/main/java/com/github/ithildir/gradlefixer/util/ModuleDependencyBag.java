/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

/**
 * @author Andrea Di Giorgi
 */
public class ModuleDependencyBag extends BaseDependencyBag {

	public ModuleDependencyBag(
		String configuration, String dependency, String group, String name,
		boolean transitive, String version) {

		super(configuration, dependency);

		_group = group;
		_name = name;
		_transitive = transitive;
		_version = version;
	}

	@Override
	public String getDifferences(DependencyBag other) {
		String differences = super.getDifferences(other);

		ModuleDependencyBag otherModuleDependencyBag =
			(ModuleDependencyBag)other;

		if (_transitive != otherModuleDependencyBag.isTransitive()) {
			differences += getDifferenceMessage(
				"transitive", String.valueOf(_transitive),
				String.valueOf(otherModuleDependencyBag.isTransitive()));
		}

		return differences;
	}

	public String getGroup() {
		return _group;
	}

	@Override
	public String getKey() {
		return String.format("%s:%s:%s", _group, _name, _version);
	}

	public String getName() {
		return _name;
	}

	public String getVersion() {
		return _version;
	}

	public boolean isTransitive() {
		return _transitive;
	}

	private final String _group;
	private final String _name;
	private final boolean _transitive;
	private final String _version;

}
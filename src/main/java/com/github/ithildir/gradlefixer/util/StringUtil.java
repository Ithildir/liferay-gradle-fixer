/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Andrea Di Giorgi
 */
public class StringUtil {

	public static boolean contains(String s, char... chars) {
		for (char c : chars) {
			if (s.indexOf(c) != -1) {
				return true;
			}
		}

		return false;
	}

	public static String join(Iterable<String> strings, String separator) {
		StringBuilder sb = new StringBuilder();

		int i = 0;

		for (String s : strings) {
			if (i > 0) {
				sb.append(separator);
			}

			sb.append(s);

			i++;
		}

		return sb.toString();
	}

	public static List<String> split(String s, String separator) {
		if (Validator.isNull(s)) {
			return Collections.emptyList();
		}

		String[] tokensArray = s.split(separator);

		List<String> tokens = new ArrayList<>(tokensArray.length);

		for (String token : tokensArray) {
			token = token.trim();

			if (Validator.isNotNull(token)) {
				tokens.add(token);
			}
		}

		return tokens;
	}

}
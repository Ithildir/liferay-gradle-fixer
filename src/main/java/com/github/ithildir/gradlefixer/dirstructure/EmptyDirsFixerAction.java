/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.dirstructure;

import com.github.ithildir.gradlefixer.FixerAction;

import java.io.IOException;

import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileSystemException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author Andrea Di Giorgi
 */
public class EmptyDirsFixerAction implements FixerAction {

	public static final EmptyDirsFixerAction INSTANCE =
		new EmptyDirsFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Files.walkFileTree(
			moduleDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(
						Path dirPath, BasicFileAttributes basicFileAttributes)
					throws IOException {

					try {
						Files.delete(dirPath);

						return FileVisitResult.SKIP_SUBTREE;
					}
					catch (DirectoryNotEmptyException dnee) {
					}
					catch (FileSystemException fse) {
						System.out.println(
							"Unable to remove " + dirPath + ": " +
								fse.getMessage());
					}

					return FileVisitResult.CONTINUE;
				}

			});
	}

	private EmptyDirsFixerAction() {
	}

}
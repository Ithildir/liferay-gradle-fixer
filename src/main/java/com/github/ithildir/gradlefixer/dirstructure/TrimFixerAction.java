/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.dirstructure;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.io.IOException;

import java.nio.file.Path;

/**
 * @author Andrea Di Giorgi
 */
public class TrimFixerAction implements FixerAction {

	public static final TrimFixerAction INSTANCE = new TrimFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		if (Validator.isNull(_fileName)) {
			throw new IllegalArgumentException("Please set -Dtrim.file.name");
		}

		Path path = moduleDirPath.resolve(_fileName);

		String content = FileUtil.read(path);

		FileUtil.write(path, content.trim());
	}

	private TrimFixerAction() {
		_fileName = System.getProperty("trim.file.name");
	}

	private final String _fileName;

}
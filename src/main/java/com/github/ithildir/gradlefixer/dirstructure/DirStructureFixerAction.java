/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.dirstructure;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrea Di Giorgi
 */
public class DirStructureFixerAction implements FixerAction {

	public static final DirStructureFixerAction INSTANCE =
		new DirStructureFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path oldTestIntegrationDirPath = moduleDirPath.resolve(
			"src/test-integration");

		if (Files.exists(oldTestIntegrationDirPath)) {
			Path newTestIntegrationDirPath = moduleDirPath.resolve(
				"src/" + _SOURCE_SET_NAMES[2]);

			Files.move(oldTestIntegrationDirPath, newTestIntegrationDirPath);
		}

		_moveSourceSetFiles(moduleDirPath, "src", _SOURCE_SET_NAMES, 0);
		_moveSourceSetFiles(
			moduleDirPath, "test/integration", _SOURCE_SET_NAMES, 2);
		_moveSourceSetFiles(moduleDirPath, "test/unit", _SOURCE_SET_NAMES, 1);

		_makeReplacements(moduleDirPath, "bnd.bnd", _bndBndReplacements);
		_makeReplacements(
			moduleDirPath, "build.gradle", _buildGradleReplacements);
		_makeReplacements(moduleDirPath, ".gitignore", _gitIgnoreReplacements);
	}

	private DirStructureFixerAction() {
	}

	private Path _getSourceSetDirPath(
		Path moduleDirPath, String sourceSetName) {

		return moduleDirPath.resolve("src/" + sourceSetName);
	}

	private void _makeReplacements(
			Path moduleDirPath, String fileName,
			Map<String, String> replacements)
		throws IOException {

		Path path = moduleDirPath.resolve(fileName);

		if (Files.notExists(path)) {
			return;
		}

		String content = FileUtil.read(path);

		if (content.contains(_REPLACEMENT_FIXME)) {
			throw new IOException(
				"Unable to do replacements on " + path +
					": fixes are still pending");
		}

		String newContent = content;

		for (Map.Entry<String, String> entry : replacements.entrySet()) {
			newContent = newContent.replace(entry.getKey(), entry.getValue());
		}

		if (!content.equals(newContent)) {
			FileUtil.write(path, newContent);
		}
	}

	private void _moveSourceSetFiles(
			final Path moduleDirPath, String sourceRootDirName,
			final String[] sourceSetNames, int sourceSetIndex)
		throws IOException {

		final Path sourceRootDirPath = moduleDirPath.resolve(sourceRootDirName);

		if (!Files.exists(sourceRootDirPath)) {
			return;
		}

		final Path sourceSetDirPath = _getSourceSetDirPath(
			moduleDirPath, sourceSetNames[sourceSetIndex]);

		Files.walkFileTree(
			sourceRootDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(
						Path dirPath, BasicFileAttributes basicFileAttributes)
					throws IOException {

					for (String curSourceSetName : sourceSetNames) {
						Path curSourceSetDirPath = _getSourceSetDirPath(
							moduleDirPath, curSourceSetName);

						if (dirPath.startsWith(curSourceSetDirPath)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}

					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(
						Path path, BasicFileAttributes basicFileAttributes)
					throws IOException {

					String extension = FileUtil.getExtension(path);

					String dirName = null;

					if (extension.equals("java")) {
						dirName = "java";
					}
					else {
						dirName = "resources";
					}

					FileUtil.move(
						path, sourceRootDirPath,
						sourceSetDirPath.resolve(dirName));

					return FileVisitResult.CONTINUE;
				}

			});
	}

	private static final boolean _REPLACE_CLASSES_DIR = false;

	private static final String _REPLACEMENT_FIXME = "???FIXME???";

	private static final String[] _SOURCE_SET_NAMES = {
		"main", "test", "testIntegration"
	};

	private static final Map<String, String> _bndBndReplacements =
		new HashMap<>();
	private static final Map<String, String> _buildGradleReplacements =
		new HashMap<>();
	private static final Map<String, String> _gitIgnoreReplacements =
		new HashMap<>();

	static {
		if (_REPLACE_CLASSES_DIR) {
			_bndBndReplacements.put("classes,\\", "build/classes/main,\\");
			_bndBndReplacements.put(
				"Include-Resource: classes",
				"Include-Resource: build/classes/main");
		}

		_bndBndReplacements.put("src/content", "src/main/resources/content");
		_bndBndReplacements.put("src/META-INF", "src/main/resources/META-INF");

		_buildGradleReplacements.put("-api/src\"", "-api/src/main/java\"");
		_buildGradleReplacements.put(
			"-test/test/integration\"", "-test/src/testIntegration/java\"");
		_buildGradleReplacements.put(
			"\"src/\"", "\"src/main/" + _REPLACEMENT_FIXME + "/\"");
		_buildGradleReplacements.put(
			"\"src/com/", "\"src/main/" + _REPLACEMENT_FIXME + "/com/");
		_buildGradleReplacements.put(
			"\"src/net/", "\"src/main/" + _REPLACEMENT_FIXME + "/net/");
		_buildGradleReplacements.put(
			"\"src/org/", "\"src/main/" + _REPLACEMENT_FIXME + "/org/");
		_buildGradleReplacements.put(
			"\"src\"", "\"src/main/" + _REPLACEMENT_FIXME + "\"");
		_buildGradleReplacements.put(
			"\"test/unit/", "\"src/test/" + _REPLACEMENT_FIXME + "/");
		_buildGradleReplacements.put(
			"src/META-INF", "src/main/resources/META-INF");

		_gitIgnoreReplacements.put(
			"/src/META-INF/", "/src/main/resources/META-INF/");
		_gitIgnoreReplacements.put(
			"/test/integration/",
			"/src/testIntegration/" + _REPLACEMENT_FIXME + "/");
		_gitIgnoreReplacements.put(
			"/test/unit/", "/src/test/" + _REPLACEMENT_FIXME + "/");
	}

}
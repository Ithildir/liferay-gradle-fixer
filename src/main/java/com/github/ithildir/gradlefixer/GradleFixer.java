/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer;

import com.github.ithildir.gradlefixer.bndbnd.BndBndFixerAction;
import com.github.ithildir.gradlefixer.bndbnd.export.ExportBndBndFixerAction;
import com.github.ithildir.gradlefixer.bndbnd.exportcontents.ExportContentsBndBndFixerAction;
import com.github.ithildir.gradlefixer.bndbnd.internal.InternalBndBndFixerAction;
import com.github.ithildir.gradlefixer.bndbnd.main.MainBndBndFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.BuildGradleDependenciesFixerAction1;
import com.github.ithildir.gradlefixer.buildgradle.BuildGradleDependenciesFixerAction2;
import com.github.ithildir.gradlefixer.buildgradle.compare.CompareBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.compendium.CompendiumBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.duplicate.DuplicateBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.include.IncludeBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.jsp.JSPBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.separaterepos.SeparateReposBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.test.TestBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.buildgradle.wsdd.WSDDBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.dirstructure.DirStructureFixerAction;
import com.github.ithildir.gradlefixer.dirstructure.EmptyDirsFixerAction;
import com.github.ithildir.gradlefixer.dirstructure.TrimFixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;

import java.io.File;
import java.io.IOException;

import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

/**
 * @author Andrea Di Giorgi
 */
public class GradleFixer {

	public static void main(String[] args) throws Exception {
		String command = args[0];
		String[] limitDirNames = new String[args.length - 2];
		String rootDirName = args[1];

		if (args.length > 2) {
			System.arraycopy(args, 2, limitDirNames, 0, limitDirNames.length);
		}

		List<String> excludeRegexesList = StringUtil.split(
			System.getProperty("exclude.regexes"), ",");

		String[] excludeRegexes = _askForPlaceholder(
			excludeRegexesList.toArray(new String[excludeRegexesList.size()]));

		command = _askForPlaceholder(command);
		limitDirNames = _askForPlaceholder(limitDirNames);
		rootDirName = _askForPlaceholder(rootDirName);

		new GradleFixer(command, rootDirName, limitDirNames, excludeRegexes);
	}

	public GradleFixer(
			String command, String rootDirName, String[] limitDirNames,
			String[] excludeRegexes)
		throws IOException {

		Path rootDirPath = Paths.get(rootDirName);

		FixerAction[] fixerActions = _fixerActionsMap.get(command);

		Path[] limitDirPaths = new Path[limitDirNames.length];

		for (int i = 0; i < limitDirNames.length; i++) {
			limitDirPaths[i] = rootDirPath.resolve(limitDirNames[i]);
		}

		PathMatcher[] excludePathMatchers =
			new PathMatcher[excludeRegexes.length];

		FileSystem fileSystem = FileSystems.getDefault();

		for (int i = 0; i < excludeRegexes.length; i++) {
			excludePathMatchers[i] = fileSystem.getPathMatcher(
				"glob:" + excludeRegexes[i]);
		}

		for (final FixerAction fixerAction : fixerActions) {
			_fixGradle(
				fixerAction, rootDirPath, limitDirPaths, excludePathMatchers);
		}

		String commitMessageTemplate = System.getProperty(
			"commit.message.template");

		if (Validator.isNotNull(commitMessageTemplate) &&
			(limitDirPaths.length > 0)) {

			for (Path dirPath : limitDirPaths) {
				_commitRoot(dirPath, commitMessageTemplate);
			}
		}
	}

	private static String _askForPlaceholder(String s) throws Exception {
		if ((s == null) || !s.contains(_PLACEHOLDER)) {
			return s;
		}

		String placeholder = null;

		Toolkit toolkit = Toolkit.getDefaultToolkit();

		Clipboard clipboard = toolkit.getSystemClipboard();

		if (clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor)) {
			placeholder = (String)clipboard.getData(DataFlavor.stringFlavor);
		}

		placeholder = JOptionPane.showInputDialog(s, placeholder);

		if ((placeholder == null) || placeholder.isEmpty()) {
			throw new IllegalArgumentException();
		}

		return s.replace(_PLACEHOLDER, placeholder);
	}

	private static String[] _askForPlaceholder(String[] strings)
		throws Exception {

		for (int i = 0; i < strings.length; i++) {
			strings[i] = _askForPlaceholder(strings[i]);
		}

		return strings;
	}

	private void _commit(Path dirPath, String message) throws IOException {
		Runtime runtime = Runtime.getRuntime();

		File dir = dirPath.toFile();

		// git add .

		Process process = runtime.exec(
			new String[] {"git", "add", "."}, null, dir);

		try {
			process.waitFor();
		}
		catch (InterruptedException ie) {
		}

		// git commit -m

		process = runtime.exec(
			new String[] {"git", "commit", "-m", message}, null, dir);

		try {
			process.waitFor();
		}
		catch (InterruptedException ie) {
		}
	}

	private void _commitRoot(Path rootDirPath, String messageTemplate)
		throws IOException {

		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(
				rootDirPath, FileUtil.directoriesOnlyFilter)) {

			Iterator<Path> iterator = directoryStream.iterator();

			while (iterator.hasNext()) {
				Path dirPath = iterator.next();

				Path dirNamePath = dirPath.getFileName();

				String message = messageTemplate.replace(
					"[$DIR_NAME$]", dirNamePath.toString());

				_commit(dirPath, message);
			}
		}
	}

	private void _fixGradle(
			final FixerAction fixerAction, final Path rootDirPath,
			final Path[] limitDirPaths, final PathMatcher[] excludePathMatchers)
		throws IOException {

		Files.walkFileTree(
			rootDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(
						Path dirPath, BasicFileAttributes basicFileAttributes)
					throws IOException {

					if ((limitDirPaths.length > 0) &&
						!dirPath.equals(rootDirPath) &&
						!FileUtil.startsWith(dirPath, limitDirPaths)) {

						return FileVisitResult.CONTINUE;
					}

					for (PathMatcher pathMatcher : excludePathMatchers) {
						if (pathMatcher.matches(dirPath)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}

					if (Files.exists(dirPath.resolve("bnd.bnd")) &&
						Files.exists(dirPath.resolve("build.gradle"))) {

						fixerAction.fix(dirPath, rootDirPath);

						return FileVisitResult.SKIP_SUBTREE;
					}

					return FileVisitResult.CONTINUE;
				}

			});
	}

	private static final String _PLACEHOLDER = "[$PLACEHOLDER$]";

	private static final Map<String, FixerAction[]> _fixerActionsMap =
		new HashMap<>();

	static {
		_fixerActionsMap.put(
			"bndBnd", new FixerAction[] {BndBndFixerAction.INSTANCE});

		_fixerActionsMap.put(
			"buildGradle",
			new FixerAction[] {
				BuildGradleDependenciesFixerAction1.INSTANCE,
				BuildGradleDependenciesFixerAction2.INSTANCE
			});

		_fixerActionsMap.put(
			"compare",
			new FixerAction[] {
				CompareBuildGradleDependenciesFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"compendium",
			new FixerAction[] {
				CompendiumBuildGradleDependenciesFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"dirStructure",
			new FixerAction[] {DirStructureFixerAction.INSTANCE});

		_fixerActionsMap.put(
			"duplicate",
			new FixerAction[] {
				DuplicateBuildGradleDependenciesFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"export",
			new FixerAction[] {
				ExportBndBndFixerAction.INSTANCE, BndBndFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"exportContents",
			new FixerAction[] {
				ExportContentsBndBndFixerAction.INSTANCE,
				BndBndFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"include",
			new FixerAction[] {
				IncludeBuildGradleDependenciesFixerAction.INSTANCE,
				BndBndFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"internal",
			new FixerAction[] {
				InternalBndBndFixerAction.INSTANCE,
				EmptyDirsFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"jsp",
			new FixerAction[] {JSPBuildGradleDependenciesFixerAction.INSTANCE});

		_fixerActionsMap.put(
			"main",
			new FixerAction[] {
				MainBndBndFixerAction.INSTANCE, BndBndFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"separateRepos",
			new FixerAction[] {
				SeparateReposBuildGradleDependenciesFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"test",
			new FixerAction[] {
				TestBuildGradleDependenciesFixerAction.INSTANCE
			});

		_fixerActionsMap.put(
			"trim", new FixerAction[] {TrimFixerAction.INSTANCE});

		_fixerActionsMap.put(
			"wsdd",
			new FixerAction[] {
				WSDDBuildGradleDependenciesFixerAction.INSTANCE
			});
	}

}
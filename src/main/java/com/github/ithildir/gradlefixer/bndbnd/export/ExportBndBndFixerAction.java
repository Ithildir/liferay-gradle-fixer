/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.bndbnd.export;

import aQute.bnd.osgi.Constants;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.PropertiesUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;

import java.io.File;
import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Andrea Di Giorgi
 */
public class ExportBndBndFixerAction implements FixerAction {

	public static final ExportBndBndFixerAction INSTANCE =
		new ExportBndBndFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		String exportPackage = PropertiesUtil.get(
			bndProperties, Constants.EXPORT_PACKAGE);

		if (exportPackage == null) {
			return;
		}

		List<String> exportPackageNames = StringUtil.split(
			exportPackage, _SEPARATOR);

		SortedSet<String> newExportPackageNames;

		if (_replacePackageWildcards) {
			newExportPackageNames = _replacePackageWildcards(
				moduleDirPath, exportPackageNames);
		}
		else {
			newExportPackageNames = _cleanUpPackages(
				moduleDirPath, exportPackageNames);
		}

		if (newExportPackageNames == null) {
			return;
		}

		if ((newExportPackageNames.size() == exportPackageNames.size()) &&
			newExportPackageNames.containsAll(exportPackageNames)) {

			return;
		}

		bndProperties.setProperty(
			Constants.EXPORT_PACKAGE,
			StringUtil.join(newExportPackageNames, _SEPARATOR));

		FileUtil.write(bndBndPath, bndProperties);

		System.out.println("Written " + bndBndPath);
	}

	private ExportBndBndFixerAction() {
		_replacePackageWildcards = Boolean.getBoolean(
			"export.replace.package.wildcards");
	}

	private void _addPackageNames(
			final Collection<String> packageNames, final Path rootDirPath)
		throws IOException {

		if (Files.notExists(rootDirPath)) {
			return;
		}

		Files.walkFileTree(
			rootDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(
						Path dirPath, BasicFileAttributes basicFileAttributes)
					throws IOException {

					if (dirPath.equals(rootDirPath)) {
						return FileVisitResult.CONTINUE;
					}

					Path dirNamePath = dirPath.getFileName();

					String dirName = dirNamePath.toString();

					if (Character.isUpperCase(dirName.charAt(0))) {
						return FileVisitResult.SKIP_SUBTREE;
					}

					if (FileUtil.hasFiles(dirPath)) {
						String packageName = FileUtil.getRelativeFileName(
							dirPath, rootDirPath);

						if (packageName.equals("content") ||
							packageName.equals("custom-sql") ||
							packageName.equals("resource-actions")) {

							return FileVisitResult.SKIP_SUBTREE;
						}

						packageName = packageName.replace('/', '.');

						packageNames.add(packageName);
					}

					return FileVisitResult.CONTINUE;
				}

			});
	}

	private SortedSet<String> _cleanUpPackages(
			Path moduleDirPath, List<String> exportPackageNames)
		throws IOException {

		SortedSet<String> newExportPackageNames = new TreeSet<>(
			exportPackageNames);

		Iterator<String> iterator = newExportPackageNames.iterator();

		while (iterator.hasNext()) {
			String packageName = iterator.next();

			if (!packageName.startsWith("com.liferay.") ||
				StringUtil.contains(packageName, '*', '!')) {

				continue;
			}

			if (!_hasFiles(moduleDirPath, packageName)) {
				iterator.remove();
			}
		}

		return newExportPackageNames;
	}

	private boolean _hasFiles(Path moduleDirPath, String packageName)
		throws IOException {

		for (String rootDirName : _ROOT_DIR_NAMES) {
			Path dirPath = moduleDirPath.resolve(
				rootDirName + File.separatorChar +
					packageName.replace('.', File.separatorChar));

			if (FileUtil.hasFiles(dirPath)) {
				return true;
			}
		}

		return false;
	}

	private SortedSet<String> _replacePackageWildcards(
			Path moduleDirPath, List<String> exportPackageNames)
		throws IOException {

		SortedSet<String> newExportPackageNames = new TreeSet<>();

		Set<String> packageNames = new HashSet<>();

		for (String rootDirName : _ROOT_DIR_NAMES) {
			_addPackageNames(packageNames, moduleDirPath.resolve(rootDirName));
		}

		for (String exportPackageName : exportPackageNames) {
			if ((exportPackageName.charAt(0) == '!') ||
				!exportPackageName.endsWith(".*")) {

				newExportPackageNames.add(exportPackageName);

				continue;
			}

			String start = exportPackageName.substring(
				0, exportPackageName.length() - 2);

			for (String packageName : packageNames) {
				if (packageName.equals(start) ||
					packageName.startsWith(start + ".")) {

					newExportPackageNames.add(packageName);
				}
			}
		}

		for (String exportPackageName : exportPackageNames) {
			if (exportPackageName.charAt(0) != '!') {
				continue;
			}

			char c = exportPackageName.charAt(exportPackageName.length() - 1);

			if (c == '*') {
				System.out.println(
					Constants.EXPORT_PACKAGE + " non supported in " +
						moduleDirPath + ": " + exportPackageName);

				return null;
			}

			boolean removed = newExportPackageNames.remove(
				exportPackageName.substring(1));

			if (removed) {
				newExportPackageNames.remove(exportPackageName);
			}
			else {
				System.out.println(
					Constants.EXPORT_PACKAGE + " seems to be useless in " +
						moduleDirPath + ": " + exportPackageName);
			}
		}

		return newExportPackageNames;
	}

	private static final String[] _ROOT_DIR_NAMES = {
		"src/main/java", "src/main/resources"
	};

	private static final String _SEPARATOR = ",";

	private final boolean _replacePackageWildcards;

}
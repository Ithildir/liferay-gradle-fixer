/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.bndbnd;

import aQute.bnd.osgi.Constants;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.io.IOException;

import java.nio.file.Path;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Andrea Di Giorgi
 */
public class BndBndFixerAction implements FixerAction {

	public static final BndBndFixerAction INSTANCE = new BndBndFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		bndProperties = _fixBndPropertyKeys(bndProperties, bndBndPath);

		_removeUnnecessaryIncludeResource(bndProperties);

		List<String> keys = new ArrayList<>(bndProperties.size());

		keys.addAll(bndProperties.stringPropertyNames());

		Collections.sort(
			keys,
			new Comparator<String>() {

				@Override
				public int compare(String s1, String s2) {
					char c1 = s1.charAt(0);
					char c2 = s2.charAt(0);

					if (Character.isUpperCase(c1) ==
							Character.isUpperCase(c2)) {

						return s1.compareTo(s2);
					}

					if (Character.isUpperCase(c1)) {
						return -1;
					}

					return 1;
				}

			});

		StringBuilder sb = new StringBuilder();

		for (String key : keys) {
			String value = bndProperties.getProperty(key);

			_appendBndProperty(sb, key, value);

			sb.append(System.lineSeparator());
		}

		if (sb.length() > 0) {
			sb.setLength(sb.length() - System.lineSeparator().length());
		}

		FileUtil.write(bndBndPath, sb.toString());
	}

	private static void _populateBndPropertyKeysMap(String... keys) {
		for (String key : keys) {
			_bndPropertyKeysMap.put(key.toLowerCase(), key);
		}
	}

	private BndBndFixerAction() {
	}

	private void _appendBndProperty(
		StringBuilder sb, String key, String value) {

		sb.append(key);
		sb.append(':');

		List<String> values = StringUtil.split(value, _SEPARATOR);

		if (values.isEmpty()) {
			return;
		}

		if (values.size() > 1) {
			sb.append('\\');

			String prevPrefix = null;

			for (String v : values) {
				int pos = v.indexOf('.');

				if ((key.equals(Constants.EXPORT_PACKAGE) ||
					 key.equals(Constants.IMPORT_PACKAGE)) &&
					(pos != -1)) {

					String prefix = v;

					pos = v.indexOf('.', pos + 1);

					if (pos != -1) {
						prefix = v.substring(0, pos);
					}

					if ((prevPrefix != null) && !prevPrefix.equals(prefix)) {
						sb.append(System.lineSeparator());
						sb.append("\t\\");
					}

					prevPrefix = prefix;
				}

				sb.append(System.lineSeparator());
				sb.append('\t');

				sb.append(v);

				sb.append(",\\");
			}
		}
		else {
			sb.append(' ');

			for (String v : values) {
				sb.append(v);
				sb.append(", ");
			}
		}

		sb.setLength(sb.length() - 2);
	}

	private Properties _fixBndPropertyKeys(
			Properties bndProperties, Path bndBndPath)
		throws IOException {

		Properties fixedBndProperties = new Properties();

		for (String key : bndProperties.stringPropertyNames()) {
			String suffix = "";

			int pos = key.indexOf('.');

			if (pos != -1) {
				key = key.substring(0, pos);

				suffix = key.substring(pos);
			}

			String correctKey = _bndPropertyKeysMap.get(key.toLowerCase());

			if (correctKey == null) {
				throw new IOException(
					"Unknown key \"" + key + "\" in " + bndBndPath);
			}

			String value = bndProperties.getProperty(key);

			fixedBndProperties.setProperty(correctKey + suffix, value);
		}

		return fixedBndProperties;
	}

	private void _removeUnnecessaryIncludeResource(Properties bndProperties) {
		String includeResource = bndProperties.getProperty(
			Constants.INCLUDERESOURCE);

		if (Validator.isNull(includeResource)) {
			return;
		}

		List<String> includedResources = StringUtil.split(
			includeResource, _SEPARATOR);

		Iterator<String> iterator = includedResources.iterator();

		while (iterator.hasNext()) {
			String includedResource = iterator.next();

			int pos = includedResource.indexOf('=');

			if (pos == -1) {
				continue;
			}

			String name = includedResource.substring(0, pos);

			if (includedResource.equals(name + "=src/main/resources/" + name)) {
				iterator.remove();
			}
		}

		bndProperties.setProperty(
			Constants.INCLUDERESOURCE,
			StringUtil.join(includedResources, _SEPARATOR));
	}

	private static final String _SEPARATOR = ",";

	private static final Map<String, String> _bndPropertyKeysMap =
		new HashMap<>();

	static {
		_populateBndPropertyKeysMap(
			"-metatype-inherit", "Can-Redefine-Classes",
			"Can-Retransform-Classes", "ConfigurationPath",
			"Implementation-Version", "JPM-Command",
			"Liferay-Export-JS-Submodules", "Liferay-JS-Config",
			"Liferay-Releng-Module-Group-Description",
			"Liferay-Releng-Module-Group-Title",
			"Liferay-Require-SchemaVersion", "Liferay-Service",
			"Liferay-Theme-Contributor-Type",
			"Liferay-Theme-Contributor-Weight", "Main-Class", "Premain-Class",
			"Web-ContextPath");
		_populateBndPropertyKeysMap(Constants.BUNDLE_SPECIFIC_HEADERS);
		_populateBndPropertyKeysMap(Constants.headers);
		_populateBndPropertyKeysMap(Constants.options);
	}

}
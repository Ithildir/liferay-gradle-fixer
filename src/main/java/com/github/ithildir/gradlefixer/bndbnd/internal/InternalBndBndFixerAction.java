/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.bndbnd.internal;

import aQute.bnd.osgi.Constants;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.FileUtil.ReplacementBag;
import com.github.ithildir.gradlefixer.util.PropertiesUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.io.File;
import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

/**
 * @author Andrea Di Giorgi
 */
public class InternalBndBndFixerAction implements FixerAction {

	public static final InternalBndBndFixerAction INSTANCE =
		new InternalBndBndFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Set<String> packageNames = new HashSet<>();

		for (String rootDirName : _ROOT_DIR_NAMES) {
			_addPackageNames(packageNames, moduleDirPath.resolve(rootDirName));
		}

		Map<String, String> packageMoves = new TreeMap<>();

		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		String bundleActivator = bndProperties.getProperty(
			Constants.BUNDLE_ACTIVATOR);

		if (Validator.isNotNull(bundleActivator) &&
			!_classExists(bundleActivator, moduleDirPath)) {

			throw new IOException(
				Constants.BUNDLE_ACTIVATOR + " in " + moduleDirPath +
					" points to a non-existent class");
		}

		Set<String> exportedPackageNames = _getExportedPackageNames(
			bndProperties);
		String rootPackageName = _getRootPackageName(bndProperties);

		for (String packageName : packageNames) {
			if (_isExported(packageName, exportedPackageNames)) {
				continue;
			}

			String notInternalPackageName = packageName.replace(
				".internal", "");

			if (_isExported(notInternalPackageName, exportedPackageNames)) {
				System.out.println(
					"Package " + packageName + " is exported as " +
						notInternalPackageName);
			}

			if (!packageName.equals(rootPackageName) &&
				!packageName.startsWith(rootPackageName + ".")) {

				StringBuilder sb = new StringBuilder();

				sb.append("Package \"");
				sb.append(packageName);
				sb.append("\" in ");
				sb.append(
					FileUtil.getRelativeFileName(moduleDirPath, rootDirPath));
				sb.append(" should start with \"");
				sb.append(rootPackageName);
				sb.append('"');

				System.out.println(sb);

				continue;
			}

			String newPackageName = rootPackageName + ".internal";

			if (packageName.equals(newPackageName) ||
				packageName.startsWith(newPackageName + ".")) {

				continue;
			}

			if (packageName.endsWith(".internal") ||
				packageName.contains(".internal.")) {

				newPackageName = JOptionPane.showInputDialog(
					"New Package Name", packageName);

				if (Validator.isNull(newPackageName)) {
					throw new IOException(
						"New package name for " + packageName +
							" must be specified");
				}
				else if (newPackageName.equals(packageName)) {
					continue;
				}
			}
			else if (packageName.length() > rootPackageName.length()) {
				newPackageName += packageName.substring(
					rootPackageName.length());
			}

			packageMoves.put(packageName, newPackageName);
		}

		if (!_doMove) {
			return;
		}

		List<ReplacementBag> replacementBags = new ArrayList<>(
			packageMoves.size());

		for (Map.Entry<String, String> entry : packageMoves.entrySet()) {
			String packageName = entry.getKey();
			String newPackageName = entry.getValue();

			String regex = Pattern.quote(packageName + ";");
			String replacement = Matcher.quoteReplacement(newPackageName + ";");

			replacementBags.add(new ReplacementBag(regex, replacement));

			for (String rootDirName : _ROOT_DIR_NAMES) {
				Map<Path, Path> pathsMap = _movePackage(
					moduleDirPath, rootDirName, packageName, newPackageName);

				_addReplacementBags(replacementBags, pathsMap);
			}
		}

		FileUtil.replace(
			moduleDirPath, replacementBags, _PACKAGE_REPLACEMENT_PATTERNS);
	}

	private InternalBndBndFixerAction() {
		_doMove = Boolean.getBoolean("internal.bnd.do.move");
	}

	private void _addPackageNames(
			final Collection<String> packageNames, final Path rootDirPath)
		throws IOException {

		if (Files.notExists(rootDirPath)) {
			return;
		}

		Files.walkFileTree(
			rootDirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(
						Path dirPath, BasicFileAttributes basicFileAttributes)
					throws IOException {

					if (dirPath.equals(rootDirPath)) {
						return FileVisitResult.CONTINUE;
					}

					Path dirNamePath = dirPath.getFileName();

					String dirName = dirNamePath.toString();

					if (Character.isUpperCase(dirName.charAt(0))) {
						return FileVisitResult.SKIP_SUBTREE;
					}

					if (FileUtil.hasFiles(dirPath)) {
						String packageName = FileUtil.getRelativeFileName(
							dirPath, rootDirPath);

						packageName = packageName.replace('/', '.');

						if (packageNames.contains(".background.task.") ||
							packageName.contains(".dependencies.") ||
							packageName.contains(".service.base.") ||
							packageName.contains(".service.http.") ||
							packageName.contains(".service.impl.") ||
							packageName.contains(".service.persistence.") ||
							packageName.endsWith(".background.task") ||
							packageName.endsWith(".dependencies") ||
							packageName.endsWith(".model.impl") ||
							packageName.endsWith(".service.base") ||
							packageName.endsWith(".service.http") ||
							packageName.endsWith(".service.impl") ||
							packageName.endsWith(".service.persistence") ||
							packageName.equals("content") ||
							packageName.equals("custom-sql") ||
							packageName.equals("resource-actions")) {

							return FileVisitResult.SKIP_SUBTREE;
						}

						if (packageName.endsWith(".configuration") &&
							FileUtil.contains(dirPath, "*Configuration.java")) {

							return FileVisitResult.SKIP_SUBTREE;
						}

						if (packageName.endsWith(".service.util") &&
							Files.exists(
								dirPath.resolve("ServiceProps.java"))) {

							return FileVisitResult.SKIP_SUBTREE;
						}

						packageNames.add(packageName);
					}

					return FileVisitResult.CONTINUE;
				}

			});
	}

	private void _addReplacementBags(
		List<ReplacementBag> replacementBags, Map<Path, Path> pathsMap) {

		for (Map.Entry<Path, Path> entry : pathsMap.entrySet()) {
			String fileTitle = _getFileTitle(entry.getKey());
			String newFileTitle = _getFileTitle(entry.getValue());

			replacementBags.add(
				new ReplacementBag(
					Pattern.quote(fileTitle),
					Matcher.quoteReplacement(newFileTitle)));
			replacementBags.add(
				new ReplacementBag(
					Pattern.quote(fileTitle.replace('/', '.')),
					Matcher.quoteReplacement(newFileTitle.replace('/', '.'))));
		}
	}

	private boolean _classExists(String classname, Path moduleDirPath) {
		classname = classname.replace('.', '/') + ".java";

		if (Files.exists(moduleDirPath.resolve("src/main/java/" + classname)) ||
			Files.exists(moduleDirPath.resolve("src/test/java/" + classname)) ||
			Files.exists(
				moduleDirPath.resolve(
					"src/testIntegration/java/" + classname))) {

			return true;
		}

		return false;
	}

	private Set<String> _getExportedPackageNames(Properties bndProperties)
		throws IOException {

		String exportPackage = PropertiesUtil.get(
			bndProperties, Constants.EXPORT_PACKAGE);

		if (Validator.isNull(exportPackage)) {
			return Collections.emptySet();
		}
		else {
			return new HashSet<>(StringUtil.split(exportPackage, ","));
		}
	}

	private String _getFileTitle(Path path) {
		String fileName = path.toString();

		if (File.separatorChar != '/') {
			fileName = fileName.replace(File.separatorChar, '/');
		}

		int index = fileName.lastIndexOf('.');

		if (index != -1) {
			fileName = fileName.substring(0, index);
		}

		return fileName;
	}

	private String _getRootPackageName(Properties bndProperties) {
		String bundleSymbolicName = PropertiesUtil.get(
			bndProperties, Constants.BUNDLE_SYMBOLICNAME);

		int index = bundleSymbolicName.lastIndexOf('.');

		String suffix = bundleSymbolicName.substring(index + 1);

		if (suffix.equals("api") || suffix.equals("impl") ||
			suffix.equals("service")) {

			return bundleSymbolicName.substring(0, index);
		}

		return bundleSymbolicName;
	}

	private boolean _isExported(
		String packageName, Set<String> exportedPackageNames) {

		if (exportedPackageNames.isEmpty()) {
			return false;
		}

		if (exportedPackageNames.contains(packageName)) {
			return true;
		}

		int index = -1;

		while ((index = packageName.indexOf('.', index + 1)) != -1) {
			String superPackageName = packageName.substring(0, index);

			if (exportedPackageNames.contains(superPackageName + ".*")) {
				return true;
			}
		}

		return false;
	}

	private Map<Path, Path> _movePackage(
			Path moduleDirPath, String rootDirName, String packageName,
			String newPackageName)
		throws IOException {

		Path rootDirPath = moduleDirPath.resolve(rootDirName);

		final Path packageDirPath = rootDirPath.resolve(
			packageName.replace('.', '/'));

		if (Files.notExists(packageDirPath)) {
			return Collections.emptyMap();
		}

		Map<Path, Path> pathsMap = new HashMap<>();

		Set<Path> paths = FileUtil.getFiles(packageDirPath);

		final Path newPackageDirPath = rootDirPath.resolve(
			newPackageName.replace('.', '/'));

		for (Path path : paths) {
			Path newPath = FileUtil.changeParentPath(
				path, packageDirPath, newPackageDirPath);

			if (Files.exists(newPath)) {
				throw new IOException(
					"File " + newPath + " moved from " + path +
						" should not exist");
			}

			Files.createDirectories(newPath.getParent());

			Files.move(path, newPath);

			pathsMap.put(
				rootDirPath.relativize(path), rootDirPath.relativize(newPath));
		}

		return pathsMap;
	}

	private static final String[] _PACKAGE_REPLACEMENT_PATTERNS = {
		"**/*.bnd", "**/*.j*", "**/*.properties", "**/*.xml"
	};

	private static final String[] _ROOT_DIR_NAMES = {
		"src/main/java", "src/main/resources"
	};

	private final boolean _doMove;

}
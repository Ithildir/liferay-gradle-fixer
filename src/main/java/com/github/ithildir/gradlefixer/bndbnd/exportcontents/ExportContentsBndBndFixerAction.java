/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.bndbnd.exportcontents;

import aQute.bnd.osgi.Constants;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.PropertiesUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;

import java.io.IOException;

import java.nio.file.Path;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Andrea Di Giorgi
 */
public class ExportContentsBndBndFixerAction implements FixerAction {

	public static final ExportContentsBndBndFixerAction INSTANCE =
		new ExportContentsBndBndFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		String exportPackage = PropertiesUtil.get(
			bndProperties, Constants.EXPORT_PACKAGE);

		if (exportPackage == null) {
			return;
		}

		List<String> exportPackageNames = StringUtil.split(
			exportPackage, _SEPARATOR);

		String exportContents = PropertiesUtil.get(
			bndProperties, Constants.EXPORT_CONTENTS);

		Set<String> exportContentsNames = new TreeSet<>(
			StringUtil.split(exportContents, _SEPARATOR));

		Iterator<String> iterator = exportPackageNames.iterator();

		while (iterator.hasNext()) {
			String packageName = iterator.next();

			if (!packageName.startsWith("com.liferay.")) {
				iterator.remove();

				exportContentsNames.add(packageName);
			}
		}

		if (exportContentsNames.isEmpty()) {
			bndProperties.remove(Constants.EXPORT_CONTENTS);
		}
		else {
			bndProperties.setProperty(
				Constants.EXPORT_CONTENTS,
				StringUtil.join(exportContentsNames, _SEPARATOR));
		}

		if (exportPackageNames.isEmpty()) {
			bndProperties.remove(Constants.EXPORT_PACKAGE);
		}
		else {
			bndProperties.setProperty(
				Constants.EXPORT_PACKAGE,
				StringUtil.join(exportPackageNames, _SEPARATOR));
		}

		FileUtil.write(bndBndPath, bndProperties);

		System.out.println("Written " + bndBndPath);
	}

	private ExportContentsBndBndFixerAction() {
	}

	private static final String _SEPARATOR = ",";

}
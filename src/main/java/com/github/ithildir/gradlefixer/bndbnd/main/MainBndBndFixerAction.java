/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.bndbnd.main;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.io.File;
import java.io.IOException;

import java.nio.file.Path;

import java.util.Properties;

/**
 * @author Andrea Di Giorgi
 */
public class MainBndBndFixerAction implements FixerAction {

	public static final MainBndBndFixerAction INSTANCE =
		new MainBndBndFixerAction();

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path path = FileUtil.getPathWithString(
			moduleDirPath, "**/*.java", "public static void main(");

		if (path == null) {
			return;
		}

		Path javaDirPath = moduleDirPath.resolve("src/main/java");

		String className = String.valueOf(javaDirPath.relativize(path));

		className = className.replace(File.separatorChar, '.');

		className = className.substring(0, className.length() - 5);

		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		String oldClassName = (String)bndProperties.setProperty(
			_MAIN_CLASS, className);

		if (Validator.isNotNull(oldClassName) &&
			!className.equals(oldClassName)) {

			System.err.println("Replaced Main-Class in " + moduleDirPath);
		}

		FileUtil.write(bndBndPath, bndProperties);

		System.out.println("Written " + bndBndPath);
	}

	private MainBndBndFixerAction() {
	}

	private static final String _MAIN_CLASS = "Main-Class";

}
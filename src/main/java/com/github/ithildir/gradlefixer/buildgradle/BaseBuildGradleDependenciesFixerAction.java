/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle;

import com.github.ithildir.gradlefixer.util.BuildGradleUtil;

import java.io.IOException;

import java.nio.file.Path;

import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;

/**
 * @author Andrea Di Giorgi
 */
public abstract class BaseBuildGradleDependenciesFixerAction
	extends BaseBuildGradleFixerAction {

	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {
	}

	protected String fix(
			String buildGradle, List<String> dependencies, Path moduleDirPath,
			Path rootDirPath)
		throws IOException {

		fix(dependencies, moduleDirPath, rootDirPath);

		return buildGradle;
	}

	@Override
	protected String fix(
			String buildGradle, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		Object[] tuple = BuildGradleUtil.getDependencies(buildGradle);

		if (tuple == null) {
			return buildGradle;
		}

		@SuppressWarnings("unchecked")
		List<String> dependencies = (List<String>)tuple[0];
		Matcher matcher = (Matcher)tuple[1];

		buildGradle = fix(
			buildGradle, dependencies, moduleDirPath, rootDirPath);

		StringBuilder sb = new StringBuilder();

		sb.append(buildGradle.substring(0, matcher.start(1)));
		sb.append(System.lineSeparator());

		String blockConfiguration = null;

		for (String dependency : sortDependencies(dependencies)) {
			int pos = dependency.indexOf(' ');

			String configuration = dependency.substring(0, pos);

			if (blockConfiguration == null) {
				blockConfiguration = configuration;
			}

			if (!blockConfiguration.equals(configuration)) {
				blockConfiguration = configuration;

				sb.append(System.lineSeparator());
			}

			sb.append('\t');
			sb.append(dependency);
			sb.append(System.lineSeparator());
		}

		sb.append(buildGradle.substring(matcher.end(1)));

		return sb.toString();
	}

	protected Iterable<String> sortDependencies(List<String> dependencies) {
		return new TreeSet<>(dependencies);
	}

}
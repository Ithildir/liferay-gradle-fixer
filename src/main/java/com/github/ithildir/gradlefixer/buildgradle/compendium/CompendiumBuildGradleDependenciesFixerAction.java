/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.compendium;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.ArrayUtil;
import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.IOException;

import java.nio.file.Path;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Andrea Di Giorgi
 */
public class CompendiumBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final CompendiumBuildGradleDependenciesFixerAction INSTANCE =
		new CompendiumBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		_addMissingDependencies(dependencies, moduleDirPath);
		_replaceCompendium(dependencies, moduleDirPath);
	}

	private CompendiumBuildGradleDependenciesFixerAction() {
		try {
			_loadCompendiumServiceVersions("org.osgi.service.component");
		}
		catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	private void _addMissingDependencies(
			List<String> dependencies, Path moduleDirPath)
		throws IOException {

		if (!dependencies.contains(_DEPENDENCY_COMPONENT) &&
			FileUtil.containsString(
				moduleDirPath, "**/*.java",
				"import org.osgi.service.component.ComponentContext")) {

			dependencies.add(_DEPENDENCY_COMPONENT);
		}
	}

	private void _loadCompendiumServiceVersions(String... excludedServices)
		throws IOException {

		Class<?> clazz = CompendiumBuildGradleDependenciesFixerAction.class;

		String json = FileUtil.read(
			clazz.getClassLoader(),
			"com/github/ithildir/gradle/fixer/buildgradle/compendium" +
				"/dependencies/org.osgi.service.json");

		JSONObject rootJSONObject = new JSONObject(json);

		JSONObject responseJSONObject = rootJSONObject.getJSONObject(
			"response");

		JSONArray docsJSONArray = responseJSONObject.getJSONArray("docs");

		for (int i = 0; i < docsJSONArray.length(); i++) {
			JSONObject jsonObject = docsJSONArray.getJSONObject(i);

			String service = jsonObject.getString("a");

			if (ArrayUtil.contains(excludedServices, service)) {
				continue;
			}

			String version = jsonObject.getString("latestVersion");

			_compendiumServiceVersions.put(service, version);
		}
	}

	private void _replaceCompendium(
			List<String> dependencies, Path moduleDirPath)
		throws IOException {

		if (!dependencies.contains(_DEPENDENCY_COMPENDIUM)) {
			return;
		}

		boolean found = false;

		for (Map.Entry<String, String> entry :
				_compendiumServiceVersions.entrySet()) {

			String service = entry.getKey();
			String version = entry.getValue();

			if (!FileUtil.containsString(
					moduleDirPath, "**/*.java", "import " + service + ".")) {

				continue;
			}

			dependencies.add(
				"provided group: \"org.osgi\", name: \"" + service +
					"\", version: \"" + version + "\"");

			found = true;
		}

		if (!found &&
			!FileUtil.containsString(
				moduleDirPath, "**/*.java", "import org.osgi.service")) {

			found = true;
		}

		if (found) {
			dependencies.remove(_DEPENDENCY_COMPENDIUM);
		}
		else {
			System.out.println(
				"Unable to remove org.osgi.compendium dependency from " +
					moduleDirPath);
		}
	}

	private static final String _DEPENDENCY_COMPENDIUM =
		"provided group: \"org.osgi\", name: \"org.osgi.compendium\", " +
			"version: \"5.0.0\"";

	private static final String _DEPENDENCY_COMPONENT =
		"provided group: \"org.osgi\", name: \"org.osgi.service.component\", " +
			"version: \"1.3.0\"";

	private final Map<String, String> _compendiumServiceVersions =
		new HashMap<>();

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.jsp;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.BuildGradleUtil;
import com.github.ithildir.gradlefixer.util.DependencyBag;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.ModuleDependencyBag;
import com.github.ithildir.gradlefixer.util.ProjectDependencyBag;

import java.io.IOException;

import java.nio.file.Path;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrea Di Giorgi
 */
public class JSPBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final JSPBuildGradleDependenciesFixerAction INSTANCE =
		new JSPBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		for (Map.Entry<String, String> entry : _dependenciesMap.entrySet()) {
			String key = entry.getKey();

			if ((_limit != null) && !key.contains(_limit)) {
				continue;
			}

			String dependency = entry.getValue();

			if (FileUtil.containsString(
					moduleDirPath, "**/*.jsp*", entry.getKey()) &&
				!_hasSameDependency(dependencies, dependency)) {

				dependencies.add(dependency);
			}
		}
	}

	private JSPBuildGradleDependenciesFixerAction() {
		_limit = System.getProperty("jsp.limit");
	}

	private boolean _hasSameDependency(
		List<String> dependencies, String dependency) {

		String moduleGroup;
		String moduleName;
		String projectName;

		DependencyBag dependencyBag = BuildGradleUtil.getDependencyBag(
			dependency);

		if (dependencyBag instanceof ModuleDependencyBag) {
			ModuleDependencyBag moduleDependencyBag =
				(ModuleDependencyBag)dependencyBag;

			moduleGroup = moduleDependencyBag.getGroup();
			moduleName = moduleDependencyBag.getName();

			projectName = moduleName.substring(moduleGroup.length() + 1);

			projectName = projectName.replace('.', '-');
		}
		else if (dependencyBag instanceof ProjectDependencyBag) {
			ProjectDependencyBag projectDependencyBag =
				(ProjectDependencyBag)dependencyBag;

			projectName = projectDependencyBag.getProject();

			projectName = projectName.substring(
				projectName.lastIndexOf(':') + 1);

			moduleGroup = "com.liferay";

			moduleName = moduleGroup + "." + projectName.replace('-', '.');
		}
		else {
			return false;
		}

		for (String curDependency : dependencies) {
			DependencyBag curDependencyBag = BuildGradleUtil.getDependencyBag(
				curDependency);

			if (curDependencyBag instanceof ModuleDependencyBag) {
				ModuleDependencyBag moduleDependencyBag =
					(ModuleDependencyBag)curDependencyBag;

				if (moduleGroup.equals(moduleDependencyBag.getGroup()) &&
					moduleName.equals(moduleDependencyBag.getName())) {

					return true;
				}
			}
			else if (curDependencyBag instanceof ProjectDependencyBag) {
				ProjectDependencyBag projectDependencyBag =
					(ProjectDependencyBag)curDependencyBag;

				String curProjectName = projectDependencyBag.getProject();

				if (curProjectName.endsWith(":" + projectName)) {
					return true;
				}
			}
		}

		return false;
	}

	private static final String _DEPENDENCY_ASSET_TAGLIB =
		"provided group: \"com.liferay\", name: \"com.liferay.asset.taglib" +
			"\", version: \"2.0.0\"";

	private static final String _DEPENDENCY_DYNAMIC_DATA_MAPPING_TAGLIB =
		"provided project(\":apps:forms-and-workflow:dynamic-data-mapping:" +
			"dynamic-data-mapping-taglib\")";

	private static final String _DEPENDENCY_FLAGS_TAGLIB =
		"provided project(\":apps:collaboration:flags:flags-taglib\")";

	private static final String _DEPENDENCY_FRONTEND_TAGLIB_WEB =
		"provided project(\":apps:foundation:frontend-taglib:frontend-taglib-" +
			"web\")";

	private static final String _DEPENDENCY_ITEM_SELECTOR_TAGLIB =
		"provided project(\":apps:collaboration:item-selector:item-selector-" +
			"taglib\")";

	private static final String _DEPENDENCY_MAP_TAGLIB =
		"provided project(\":apps:foundation:map:map-taglib\")";

	private static final String _DEPENDENCY_PORTAL_IMPL =
		"provided group: \"com.liferay.portal\", name: " +
			"\"com.liferay.portal.impl\", version: \"2.0.0\"";

	private static final String _DEPENDENCY_PORTAL_KERNEL =
		"provided group: \"com.liferay.portal\", name: " +
			"\"com.liferay.portal.kernel\", version: \"2.0.0\"";

	private static final String _DEPENDENCY_PORTLET_API =
		"provided group: \"javax.portlet\", name: \"portlet-api\", version: " +
			"\"2.0\"";

	private static final String _DEPENDENCY_REGISTRY_API =
		"provided project(\":core:registry-api\")";

	private static final String _DEPENDENCY_STAGING_TAGLIB =
		"provided project(\":apps:web-experience:staging:staging-taglib\")";

	private static final String _DEPENDENCY_TRASH_TAGLIB =
		"provided project(\":apps:web-experience:trash:trash-taglib\")";

	private static final String _DEPENDENCY_UTIL_TAGLIB =
		"provided group: \"com.liferay.portal\", name: " +
			"\"com.liferay.util.taglib\", version: \"2.0.0\"";

	private static final Map<String, String> _dependenciesMap = new HashMap<>();

	static {
		_dependenciesMap.put(
			"import=\"com.liferay.portal.kernel.", _DEPENDENCY_PORTAL_KERNEL);
		_dependenciesMap.put(
			"import=\"com.liferay.portal.model.", _DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"com.liferay.portal.security.sso.OpenSSOUtil",
			_DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"com.liferay.portal.security.sso.SSOUtil",
			_DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"com.liferay.portal.service.", _DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"com.liferay.portal.util.", _DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"com.liferay.portlet.", _DEPENDENCY_PORTAL_IMPL);
		_dependenciesMap.put(
			"import=\"javax.portlet.", _DEPENDENCY_PORTLET_API);
		_dependenciesMap.put(
			"liferay-ui:input-editor", _DEPENDENCY_REGISTRY_API);

		_dependenciesMap.put(
			"prefix=\"liferay-asset\"", _DEPENDENCY_ASSET_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-ddm\"", _DEPENDENCY_DYNAMIC_DATA_MAPPING_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-flags\"", _DEPENDENCY_FLAGS_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-frontend\"", _DEPENDENCY_FRONTEND_TAGLIB_WEB);
		_dependenciesMap.put(
			"prefix=\"liferay-item-selector\"",
			_DEPENDENCY_ITEM_SELECTOR_TAGLIB);
		_dependenciesMap.put("prefix=\"liferay-map\"", _DEPENDENCY_MAP_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-portlet\"", _DEPENDENCY_UTIL_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-staging\"", _DEPENDENCY_STAGING_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-theme\"", _DEPENDENCY_UTIL_TAGLIB);
		_dependenciesMap.put(
			"prefix=\"liferay-trash\"", _DEPENDENCY_TRASH_TAGLIB);
		_dependenciesMap.put("prefix=\"liferay-ui\"", _DEPENDENCY_UTIL_TAGLIB);

		_dependenciesMap.put(
			"taglib uri=\"http://java.sun.com/portlet_2_0\"",
			_DEPENDENCY_UTIL_TAGLIB);
	}

	private final String _limit;

}
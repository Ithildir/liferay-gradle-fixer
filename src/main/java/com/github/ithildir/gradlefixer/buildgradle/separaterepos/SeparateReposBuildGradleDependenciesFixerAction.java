/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.separaterepos;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;

import java.io.IOException;

import java.nio.file.Path;

import java.util.List;

/**
 * @author Andrea Di Giorgi
 */
public class SeparateReposBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final SeparateReposBuildGradleDependenciesFixerAction
		INSTANCE = new SeparateReposBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		for (int i = 0; i < dependencies.size(); i++) {
			String dependency = dependencies.get(i);

			dependency = _removeProjectPath(dependency);

			dependencies.set(i, dependency);
		}
	}

	private SeparateReposBuildGradleDependenciesFixerAction() {
	}

	private String _removeProjectPath(String dependency) {
		if (!dependency.contains(" project(\"")) {
			return dependency;
		}

		int start = dependency.indexOf(':');
		int end = dependency.lastIndexOf(':');

		if (start == end) {
			return dependency;
		}

		return dependency.substring(0, start) + dependency.substring(end);
	}

}
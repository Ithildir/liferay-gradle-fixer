/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.compare;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.BuildGradleUtil;
import com.github.ithildir.gradlefixer.util.DependencyBag;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.Validator;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrea Di Giorgi
 */
public class CompareBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final CompareBuildGradleDependenciesFixerAction INSTANCE =
		new CompareBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		Path otherModuleDirPath = FileUtil.changeParentPath(
			moduleDirPath.resolve("build.gradle"), rootDirPath,
			Paths.get(_otherRootDir));

		if (Files.notExists(otherModuleDirPath)) {
			System.out.println("Unable to find " + otherModuleDirPath);

			return;
		}

		Map<String, DependencyBag> dependencyBags = _getDependencyBags(
			dependencies);

		List<String> otherDependencies = BuildGradleUtil.getDependencies(
			otherModuleDirPath);

		Map<String, DependencyBag> otherDependencyBags = _getDependencyBags(
			otherDependencies);

		for (Map.Entry<String, DependencyBag> entry :
				dependencyBags.entrySet()) {

			String key = entry.getKey();
			DependencyBag dependencyBag = entry.getValue();

			DependencyBag otherDependencyBag = otherDependencyBags.get(key);

			if (otherDependencyBag == null) {
				_printDependencyDifference(moduleDirPath, key, "removed");

				continue;
			}

			String differences = dependencyBag.getDifferences(
				otherDependencyBag);

			if (Validator.isNotNull(differences)) {
				_printDependencyDifference(moduleDirPath, key, differences);
			}
		}

		for (String key : otherDependencyBags.keySet()) {
			if (!dependencyBags.containsKey(key)) {
				_printDependencyDifference(moduleDirPath, key, "added");
			}
		}
	}

	private CompareBuildGradleDependenciesFixerAction() {
		_otherRootDir = System.getProperty("compare.other.root");
	}

	private Map<String, DependencyBag> _getDependencyBags(
		Iterable<String> dependencies) {

		Map<String, DependencyBag> dependencyBags = new HashMap<>();

		for (String dependency : dependencies) {
			try {
				DependencyBag dependencyBag = BuildGradleUtil.getDependencyBag(
					dependency);

				dependencyBags.put(dependencyBag.getKey(), dependencyBag);
			}
			catch (IllegalArgumentException iae) {
			}
		}

		return dependencyBags;
	}

	private void _printDependencyDifference(
		Path moduleDirPath, String key, String difference) {

		StringBuilder sb = new StringBuilder();

		sb.append(moduleDirPath.getFileName());
		sb.append(' ');
		sb.append(key);
		sb.append(": ");
		sb.append(difference.trim());

		System.out.println(sb);
	}

	private final String _otherRootDir;

}
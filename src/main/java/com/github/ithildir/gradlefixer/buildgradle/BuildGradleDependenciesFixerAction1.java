/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle;

import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrea Di Giorgi
 */
public class BuildGradleDependenciesFixerAction1
	extends BaseBuildGradleDependenciesFixerAction {

	public static final BuildGradleDependenciesFixerAction1 INSTANCE =
		new BuildGradleDependenciesFixerAction1();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		String bndBnd = null;

		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		if (Files.exists(bndBndPath)) {
			bndBnd = FileUtil.read(bndBndPath);

			_addIncludeResourceDependencies(dependencies, bndBnd);
		}

		_fixDependenciesConfigurations(dependencies, moduleDirPath);
		_removeUselessDependencies(dependencies, moduleDirPath);
		_substituteDependencies(dependencies, bndBnd);

		if (FileUtil.contains(
				moduleDirPath, "docroot/**/*.jsp", "src/**/*.jsp")) {

			_addJSPDependencies(dependencies);
		}

		Path initJSPPath = moduleDirPath.resolve(
			"src/META-INF/resources/init.jsp");

		if (Files.exists(initJSPPath)) {
			_addTLDDependencies(dependencies, initJSPPath, moduleDirPath);
		}

		if (FileUtil.containsString(
				moduleDirPath, "**/*.java",
				"import com.liferay.portal.service.http.TunnelUtil;")) {

			dependencies.add("compile " + _DEPENDENCY_PORTAL_IMPL);
		}
	}

	private BuildGradleDependenciesFixerAction1() {
	}

	private void _addIncludeResourceDependencies(
		List<String> dependencies, String bndBnd) {

		for (String jarName : _PORTAL_JAR_NAMES) {
			if (bndBnd.contains(jarName + ".jar")) {
				dependencies.add(
					"provided group: \"com.liferay.portal\", name: \"" +
						jarName + "\", version: liferay.portalVersion");
			}
		}
	}

	private void _addJSPDependencies(List<String> dependencies) {
		for (String jspDependency : _JSP_DEPENDENCIES) {
			if (!dependencies.contains(jspDependency)) {
				dependencies.add(jspDependency);
			}
		}
	}

	private void _addTLDDependencies(
			List<String> dependencies, Path initJSPPath, Path moduleDirPath)
		throws IOException {

		String initJSP = FileUtil.read(initJSPPath);

		File dir = moduleDirPath.toFile();

		String projectName = dir.getName();

		for (Map.Entry<String, String> entry : _tldDependencies.entrySet()) {
			String tldDependency = entry.getKey();
			String dependency = entry.getValue();

			if (initJSP.contains(tldDependency) &&
				!dependencies.contains(dependency)) {

				String dependencyProjectName = _getDependencyProjectName(
					dependency);

				if (!projectName.equals(dependencyProjectName)) {
					dependencies.add(dependency);
				}
			}
		}
	}

	private void _fixDependenciesConfigurations(
			List<String> dependencies, Path moduleDirPath)
		throws IOException {

		Path srcDirPath = moduleDirPath.resolve("src");

		boolean hasTestIntegration = _hasTestDir(moduleDirPath, "integration");
		boolean hasTestUnit = _hasTestDir(moduleDirPath, "unit");

		String newCompileConfiguration = null;

		if (!FileUtil.contains(srcDirPath, "**/*.java") &&
			(hasTestIntegration || hasTestUnit)) {

			newCompileConfiguration = "testCompile";

			if (!hasTestUnit) {
				newCompileConfiguration = "testIntegrationCompile";
			}
		}

		for (int i = 0; i < dependencies.size(); i++) {
			String dependency = dependencies.get(i);

			if (dependency.startsWith("compile ") &&
				(newCompileConfiguration != null)) {

				dependencies.set(
					i, newCompileConfiguration + dependency.substring(7));
			}
			else if (dependency.startsWith("testCompile ") && !hasTestUnit) {
				dependencies.set(
					i, "testIntegrationCompile" + dependency.substring(11));
			}
			else if (dependency.startsWith("testIntegrationCompile ") &&
					 !hasTestIntegration) {

				dependencies.set(
					i, "testIntegrationCompile" + dependency.substring(22));
			}
		}
	}

	private String _getDependencyProjectName(String dependency) {
		if (!dependency.contains(" project(\"")) {
			return null;
		}

		int pos = dependency.lastIndexOf(':');

		return dependency.substring(pos + 1, dependency.length() - 2);
	}

	private boolean _hasTestDir(Path moduleDirPath, String testType) {
		Path testDirPath = moduleDirPath.resolve("test");

		Path testTypeDirPath = testDirPath.resolve(testType);

		if (Files.exists(testTypeDirPath)) {
			return true;
		}

		return false;
	}

	private void _removeUselessDependencies(
			List<String> dependencies, Path moduleDirPath)
		throws IOException {

		for (String dependency : _USELESS_DEPENDENCIES) {
			dependencies.remove(dependency);
		}

		if ((dependencies.contains("compile " + _DEPENDENCY_JUNIT_411) ||
			 dependencies.contains("compile " + _DEPENDENCY_JUNIT_412)) &&
			!FileUtil.containsString(
				moduleDirPath, "src/**/*.java", "import org.junit")) {

			dependencies.remove("compile " + _DEPENDENCY_JUNIT_411);
			dependencies.remove("compile " + _DEPENDENCY_JUNIT_412);
		}

		boolean hasTestIntegration = _hasTestDir(moduleDirPath, "integration");
		boolean hasTestUnit = _hasTestDir(moduleDirPath, "unit");

		Iterator<String> iterator = dependencies.iterator();

		while (iterator.hasNext()) {
			String dependency = iterator.next();

			if ((!hasTestIntegration &&
				 dependency.startsWith("testIntegrationCompile ")) ||
				(!hasTestUnit && dependency.startsWith("testCompile "))) {

				iterator.remove();
			}
		}
	}

	private void _substituteDependencies(
			List<String> dependencies, String bndBnd)
		throws IOException {

		Map<String, String> dependencySubstitutions = new HashMap<>(
			_dependencySubstitutions);

		if (bndBnd != null) {
			if (!bndBnd.contains("portal-service.jar")) {
				dependencySubstitutions.put(
					"provided " + _DEPENDENCY_PORTAL_SERVICE,
					"compile " + _DEPENDENCY_PORTAL_SERVICE);
			}

			for (String dependency : dependencies) {
				if (dependency.startsWith("provided ")) {
					continue;
				}

				Matcher matcher = _dependencyNamePattern.matcher(dependency);

				if (!matcher.find()) {
					continue;
				}

				String dependencyName = matcher.group(1);

				if (bndBnd.contains(dependencyName + ".jar")) {
					int pos = dependency.indexOf(' ');

					dependencySubstitutions.put(
						dependency,
						"provided " + dependency.substring(pos + 1));
				}
			}
		}

		for (Map.Entry<String, String> entry :
				dependencySubstitutions.entrySet()) {

			boolean found = dependencies.remove(entry.getKey());

			if (found) {
				dependencies.add(entry.getValue());
			}
		}
	}

	private static final String _DEPENDENCY_ARQUILLIAN_JUNIT_BRIDGE =
		"group: \"com.liferay\", name: " +
			"\"com.liferay.arquillian.extension.junit.bridge\", version: " +
				"\"1.0.0-SNAPSHOT\"";

	private static final String _DEPENDENCY_JSP =
		"group: \"javax.servlet.jsp\", name: \"jsp-api\", version: \"2.1\"";

	private static final String _DEPENDENCY_JUNIT_411 =
		"group: \"junit\", name: \"junit\", version: \"4.11\"";

	private static final String _DEPENDENCY_JUNIT_412 =
		"group: \"junit\", name: \"junit\", version: \"4.12\"";

	private static final String _DEPENDENCY_OSGI_SERVICE_TRACKER_MAP =
		"project(\":core:osgi-service-tracker-map\")";

	private static final String _DEPENDENCY_PORTAL_IMPL =
		"group: \"com.liferay.portal\", name: \"portal-impl\", version: " +
			"liferay.portalVersion";

	private static final String _DEPENDENCY_PORTAL_SERVICE =
		"group: \"com.liferay.portal\", name: \"portal-service\", version: " +
			"liferay.portalVersion";

	private static final String _DEPENDENCY_PORTAL_TEST =
		"group: \"com.liferay.portal\", name: \"portal-test\", version: " +
			"liferay.portalVersion";

	private static final String _DEPENDENCY_PORTAL_TEST_INTERNAL =
		"group: \"com.liferay.portal\", name: \"portal-test-internal\", " +
			"version: liferay.portalVersion";

	private static final String _DEPENDENCY_PORTLET =
		"group: \"javax.portlet\", name: \"portlet-api\", version: \"2.0\"";

	private static final String _DEPENDENCY_REGISTRY_API =
		"project(\":core:registry-api\")";

	private static final String _DEPENDENCY_SERVLET =
		"group: \"javax.servlet\", name: \"javax.servlet-api\", version: " +
			"\"3.0.1\"";

	private static final String[] _JSP_DEPENDENCIES = {
		"compile group: \"com.liferay.portal\", name: \"util-taglib\", " +
			"version: liferay.portalVersion",
		"compile " + _DEPENDENCY_JSP, "compile " + _DEPENDENCY_PORTLET,
		"compile " + _DEPENDENCY_SERVLET
	};

	private static final String[] _PORTAL_JAR_NAMES = {
		"portal-impl", "portal-service", "portal-test", "portal-test-internal",
		"util-bridges", "util-java", "util-slf4j", "util-taglib"
	};

	private static final String[] _USELESS_DEPENDENCIES = {
		"testCompile " + _DEPENDENCY_JUNIT_411,
		"testCompile " + _DEPENDENCY_JUNIT_412,
		"testCompile " + _DEPENDENCY_PORTAL_TEST,
		"testCompile " + _DEPENDENCY_PORTAL_TEST_INTERNAL,
		"testIntegrationCompile " + _DEPENDENCY_JUNIT_411,
		"testIntegrationCompile " + _DEPENDENCY_JUNIT_412,
		"testIntegrationCompile " + _DEPENDENCY_PORTAL_TEST,
		"testIntegrationCompile " + _DEPENDENCY_PORTAL_TEST_INTERNAL
	};

	private static final Pattern _dependencyNamePattern = Pattern.compile(
		"name: \"(.+?)\"");
	private static final Map<String, String> _dependencySubstitutions =
		new HashMap<>();
	private static final Map<String, String> _tldDependencies = new HashMap<>();

	static {
		_dependencySubstitutions.put(
			"provided " + _DEPENDENCY_JSP, "compile " + _DEPENDENCY_JSP);
		_dependencySubstitutions.put(
			"provided " + _DEPENDENCY_OSGI_SERVICE_TRACKER_MAP,
			"compile " + _DEPENDENCY_OSGI_SERVICE_TRACKER_MAP);
		_dependencySubstitutions.put(
			"provided " + _DEPENDENCY_PORTLET,
			"compile " + _DEPENDENCY_PORTLET);
		_dependencySubstitutions.put(
			"provided " + _DEPENDENCY_REGISTRY_API,
			"compile " + _DEPENDENCY_REGISTRY_API);
		_dependencySubstitutions.put(
			"provided " + _DEPENDENCY_SERVLET,
			"compile " + _DEPENDENCY_SERVLET);
		_dependencySubstitutions.put(
			"testCompile " + _DEPENDENCY_ARQUILLIAN_JUNIT_BRIDGE,
			"testIntegrationCompile " + _DEPENDENCY_ARQUILLIAN_JUNIT_BRIDGE);

		_tldDependencies.put(
			"taglib uri=\"http://liferay.com/tld/application-list\"",
			"compile project(\":apps:application-list:application-list-" +
				"taglib\")");
		_tldDependencies.put(
			"taglib uri=\"http://liferay.com/tld/ddm\" prefix=\"liferay-ddm\"",
			"compile project(\":apps:dynamic-data-mapping:dynamic-data-" +
				"mapping-taglib\")");
		_tldDependencies.put(
			"taglib uri=\"http://liferay.com/tld/item-selector\"",
			"compile project(\":apps:item-selector:item-selector-taglib\")");
	}

}
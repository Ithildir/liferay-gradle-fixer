/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle;

import com.github.ithildir.gradlefixer.util.BuildGradleUtil;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Andrea Di Giorgi
 */
public class BuildGradleDependenciesFixerAction2
	extends BaseBuildGradleDependenciesFixerAction {

	public static final BuildGradleDependenciesFixerAction2 INSTANCE =
		new BuildGradleDependenciesFixerAction2();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		_removeRedundantDependencies(
			dependencies, "compile", moduleDirPath, rootDirPath, new String[0]);
		_removeRedundantDependencies(
			dependencies, "testCompile", moduleDirPath, rootDirPath,
			new String[] {"compile"});
		_removeRedundantDependencies(
			dependencies, "testIntegrationCompile", moduleDirPath, rootDirPath,
			new String[] {"compile", "testCompile"});
	}

	private BuildGradleDependenciesFixerAction2() {
	}

	private void _addAllDependencies(
			Set<String> allDependencies, Path moduleDirPath,
			String configuration, boolean includeDir, Path rootDirPath,
			boolean trimConfiguration)
		throws IOException {

		Path buildGradlePath = moduleDirPath.resolve("build.gradle");

		if (Files.notExists(buildGradlePath)) {
			return;
		}

		List<String> dependencies = BuildGradleUtil.getDependencies(
			buildGradlePath);

		for (String dependency : dependencies) {
			if (!dependency.startsWith(configuration + " ")) {
				continue;
			}

			if (dependency.startsWith(configuration + " project(")) {
				String projectDirName = dependency.substring(
					configuration.length() + 11, dependency.length() - 2);

				projectDirName = projectDirName.replace(':', '/');

				Path projectDirPath = rootDirPath.resolve(projectDirName);

				_addAllDependencies(
					allDependencies, projectDirPath, "compile", true,
					rootDirPath, trimConfiguration);
			}
			else if (includeDir) {
				if (trimConfiguration) {
					allDependencies.add(
						dependency.substring(configuration.length() + 1));
				}
				else {
					allDependencies.add(dependency);
				}
			}
		}
	}

	private void _removeRedundantDependencies(
			List<String> dependencies, String configuration, Path moduleDirPath,
			Path rootDirPath, String[] superConfigurations)
		throws IOException {

		Set<String> superDependencies = new HashSet<>();

		_addAllDependencies(
			superDependencies, moduleDirPath, configuration, false, rootDirPath,
			true);

		for (String superConfiguration : superConfigurations) {
			_addAllDependencies(
				superDependencies, moduleDirPath, superConfiguration, true,
				rootDirPath, true);
		}

		Iterator<String> iterator = dependencies.iterator();

		while (iterator.hasNext()) {
			String dependency = iterator.next();

			if (!dependency.startsWith(configuration + " ")) {
				continue;
			}

			dependency = dependency.substring(configuration.length() + 1);

			if (superDependencies.contains(dependency)) {
				iterator.remove();
			}
		}
	}

}
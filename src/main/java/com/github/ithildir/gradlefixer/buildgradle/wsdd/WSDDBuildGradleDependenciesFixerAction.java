/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.wsdd;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.List;

/**
 * @author Andrea Di Giorgi
 */
public class WSDDBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final WSDDBuildGradleDependenciesFixerAction INSTANCE =
		new WSDDBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		if (!_areDependenciesRequired(moduleDirPath)) {
			return;
		}

		if (!dependencies.contains(_DEPENDENCY_PORTLET_API)) {
			dependencies.add(_DEPENDENCY_PORTLET_API);
		}

		if (!dependencies.contains(_DEPENDENCY_REGISTRY_API) &&
			!dependencies.contains(_DEPENDENCY_REGISTRY_API_PROJECT)) {

			dependencies.add(_DEPENDENCY_REGISTRY_API);
		}

		if (!dependencies.contains(_DEPENDENCY_SERVLET_API)) {
			dependencies.add(_DEPENDENCY_SERVLET_API);
		}
	}

	private WSDDBuildGradleDependenciesFixerAction() {
	}

	private boolean _areDependenciesRequired(Path moduleDirPath)
		throws IOException {

		Path serviceXmlPath = moduleDirPath.resolve("service.xml");

		if (Files.notExists(serviceXmlPath)) {
			return false;
		}

		String serviceXml = FileUtil.read(serviceXmlPath);

		if (!serviceXml.contains("remote-service=\"true\"")) {
			return false;
		}

		if (FileUtil.containsString(
				moduleDirPath, "**/*[!l]ServiceImpl.java",
				"ServiceContext serviceContext")) {

			return true;
		}

		return false;
	}

	private static final String _DEPENDENCY_PORTLET_API =
		"provided group: \"javax.portlet\", name: \"portlet-api\", version: " +
			"\"2.0\"";

	private static final String _DEPENDENCY_REGISTRY_API =
		"provided group: \"com.liferay\", name: " +
			"\"com.liferay.registry.api\", version: \"1.0.0\"";

	private static final String _DEPENDENCY_REGISTRY_API_PROJECT =
		"provided project(\":core:registry-api\")";

	private static final String _DEPENDENCY_SERVLET_API =
		"provided group: \"javax.servlet\", name: \"javax.servlet-api\", " +
			"version: \"3.0.1\"";

}
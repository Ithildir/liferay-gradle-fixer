/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle;

import com.github.ithildir.gradlefixer.FixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.IOException;

import java.nio.file.Path;

/**
 * @author Andrea Di Giorgi
 */
public abstract class BaseBuildGradleFixerAction implements FixerAction {

	@Override
	public void fix(Path moduleDirPath, Path rootDirPath) throws IOException {
		Path buildGradlePath = moduleDirPath.resolve("build.gradle");

		String buildGradle = FileUtil.read(buildGradlePath);

		String newBuildGradle = fix(buildGradle, moduleDirPath, rootDirPath);

		if (!buildGradle.equals(newBuildGradle)) {
			FileUtil.write(buildGradlePath, newBuildGradle);
		}
	}

	protected abstract String fix(
			String buildGradle, Path moduleDirPath, Path rootDirPath)
		throws IOException;

}
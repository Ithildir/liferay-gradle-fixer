/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.test;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;

import java.io.File;
import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Andrea Di Giorgi
 */
public class TestBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final TestBuildGradleDependenciesFixerAction INSTANCE =
		new TestBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		if (_dependencyImports.isEmpty()) {
			_dependencyImports.put(
				_DEPENDENCY_PORTAL_IMPL,
				_getImports(rootDirPath.resolve("../portal-impl/src")));
			_dependencyImports.put(
				_DEPENDENCY_PORTAL_KERNEL,
				_getImports(rootDirPath.resolve("../portal-kernel/src")));
			_dependencyImports.put(
				_DEPENDENCY_PORTAL_TEST,
				_getImports(rootDirPath.resolve("../portal-test/src")));
			_dependencyImports.put(
				_DEPENDENCY_PORTAL_TEST_INTEGRATION,
				_getImports(
					rootDirPath.resolve("../portal-test-integration/src")));
			_dependencyImports.put(
				_DEPENDENCY_REGISTRY_API,
				_getImports(
					rootDirPath.resolve("core/registry-api/src/main/java")));
		}

		for (Map.Entry<String, String[]> entry :
				_dependencyImports.entrySet()) {

			String dependency = entry.getKey();
			String[] imports = entry.getValue();

			if (!dependencies.contains(
					_CONFIGURATION_PROVIDED + " " + dependency) &&
				!dependencies.contains(
					_CONFIGURATION_TEST_COMPILE + " " + dependency) &&
				FileUtil.containsString(
					moduleDirPath, "src/test/java/**/*.java", imports)) {

				dependencies.add(
					_CONFIGURATION_TEST_COMPILE + " " + dependency);
			}
		}
	}

	private String[] _getImports(final Path dirPath) throws IOException {
		final Set<String> imports = new HashSet<>();

		Files.walkFileTree(
			dirPath,
			new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(
					Path path, BasicFileAttributes basicFileAttributes) {

					String extension = FileUtil.getExtension(path);

					if (!extension.equals("java")) {
						return FileVisitResult.CONTINUE;
					}

					Path relativePath = dirPath.relativize(path);

					String className = relativePath.toString();

					className = className.replace(File.separatorChar, '.');
					className = className.substring(0, className.length() - 5);

					imports.add("import " + className);

					return FileVisitResult.CONTINUE;
				}

			});

		return imports.toArray(new String[imports.size()]);
	}

	private static final String _CONFIGURATION_PROVIDED = "provided";

	private static final String _CONFIGURATION_TEST_COMPILE = "testCompile";

	private static final String _DEPENDENCY_PORTAL_IMPL =
		"group: \"com.liferay.portal\", name: \"com.liferay.portal.impl\", " +
			"version: \"2.0.0\"";

	private static final String _DEPENDENCY_PORTAL_KERNEL =
		"group: \"com.liferay.portal\", name: \"com.liferay.portal.kernel\", " +
			"version: \"2.0.0\"";

	private static final String _DEPENDENCY_PORTAL_TEST =
		"group: \"com.liferay.portal\", name: \"com.liferay.portal.test\", " +
			"version: \"default\"";

	private static final String _DEPENDENCY_PORTAL_TEST_INTEGRATION =
		"group: \"com.liferay.portal\", name: " +
			"\"com.liferay.portal.test.integration\", version: \"default\"";

	private static final String _DEPENDENCY_REGISTRY_API =
		"group: \"com.liferay\", name: \"com.liferay.registry.api\", " +
			"version: \"1.0.0\"";

	private final Map<String, String[]> _dependencyImports = new HashMap<>();

}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.include;

import aQute.bnd.osgi.Constants;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.StringUtil;

import java.io.IOException;

import java.nio.file.Path;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrea Di Giorgi
 */
public class IncludeBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final IncludeBuildGradleDependenciesFixerAction INSTANCE =
		new IncludeBuildGradleDependenciesFixerAction();

	@Override
	protected String fix(
			String buildGradle, List<String> dependencies, Path moduleDirPath,
			Path rootDirPath)
		throws IOException {

		Path bndBndPath = moduleDirPath.resolve("bnd.bnd");

		Properties bndProperties = FileUtil.readProperties(bndBndPath);

		AtomicReference<Boolean> expandCompileInclude = new AtomicReference<>(
			null);

		List<String> bundleClasspath = StringUtil.split(
			bndProperties.getProperty(Constants.BUNDLE_CLASSPATH), ",");
		List<String> includeResource = StringUtil.split(
			bndProperties.getProperty(Constants.INCLUDERESOURCE), ",");
		List<String> headerIncludeResource = StringUtil.split(
			bndProperties.getProperty(Constants.INCLUDE_RESOURCE), ",");

		try {
			_convertIncludedResources(
				includeResource, bundleClasspath, dependencies,
				expandCompileInclude);

			_convertIncludedResources(
				headerIncludeResource, bundleClasspath, dependencies,
				expandCompileInclude);
		}
		catch (Exception e) {
			System.err.println(
				"Error while processing " + moduleDirPath + ": " +
					e.getMessage());

			return buildGradle;
		}

		Boolean curExpandCompileInclude = expandCompileInclude.get();

		if (curExpandCompileInclude == null) {
			return buildGradle;
		}

		if ((bundleClasspath.size() == 1) &&
			".".equals(bundleClasspath.get(0))) {

			bundleClasspath.clear();
		}

		_replacePropertyValue(
			bndProperties, Constants.BUNDLE_CLASSPATH, bundleClasspath);
		_replacePropertyValue(
			bndProperties, Constants.INCLUDERESOURCE, includeResource);
		_replacePropertyValue(
			bndProperties, Constants.INCLUDE_RESOURCE, headerIncludeResource);

		FileUtil.write(bndBndPath, bndProperties);

		if (!curExpandCompileInclude.booleanValue()) {
			if (!buildGradle.contains("expandCompileInclude = false")) {
				StringBuilder sb = new StringBuilder(buildGradle);

				sb.append(System.lineSeparator());
				sb.append(System.lineSeparator());

				sb.append("liferayOSGi {");
				sb.append(System.lineSeparator());

				sb.append("\texpandCompileInclude = false");
				sb.append(System.lineSeparator());

				sb.append('}');

				buildGradle = sb.toString();
			}
		}

		return buildGradle;
	}

	private IncludeBuildGradleDependenciesFixerAction() {
	}

	private void _convertDependencyConfiguration(
			List<String> dependencies, String name)
		throws Exception {

		for (int i = 0; i < dependencies.size(); i++) {
			String dependency = dependencies.get(i);

			if (dependency.contains(", name: \"" + name + "\"")) {
				dependency =
					_COMPILE_INCLUDE_CONFIGURATION +
						dependency.substring(dependency.indexOf(' '));

				dependency = dependency.replaceFirst(
					Pattern.quote(", transitive: false,"),
					Matcher.quoteReplacement(","));

				dependencies.set(i, dependency);

				return;
			}
		}

		throw new Exception(
			"Unable to find dependency with name \"" + name + "\"");
	}

	private void _convertIncludedResources(
			List<String> includeResource, List<String> bundleClasspath,
			List<String> dependencies,
			AtomicReference<Boolean> expandCompileInclude)
		throws Exception {

		Iterator<String> iterator = includeResource.iterator();

		while (iterator.hasNext()) {
			String resource = iterator.next();

			if (resource.contains("!") || !resource.contains(".jar") ||
				resource.contains("../")) {

				continue;
			}

			boolean resourceExpandCompileInclude = false;

			if (resource.charAt(0) == '@') {
				resourceExpandCompileInclude = true;
			}

			Boolean prevExpandCompileInclude = expandCompileInclude.getAndSet(
				resourceExpandCompileInclude);

			if ((prevExpandCompileInclude != null) &&
				(prevExpandCompileInclude.booleanValue() !=
					resourceExpandCompileInclude)) {

				throw new UnsupportedOperationException(
					"Unable to mix expanded and not expanded dependencies");
			}

			int start = 1;

			if (!resourceExpandCompileInclude) {
				start = resource.indexOf('=') + 1;
			}

			Matcher matcher = _endPattern.matcher(resource);

			if (!matcher.find()) {
				throw new Exception("Unparseable resource " + resource);
			}

			int end = matcher.start();

			String name = resource.substring(start, end);

			_convertDependencyConfiguration(dependencies, name);

			iterator.remove();

			if (!resourceExpandCompileInclude) {
				boolean removed = bundleClasspath.remove(
					resource.substring(0, start - 1));

				if (!removed) {
					throw new Exception(
						"Unable to find " + resource + " in " +
							Constants.BUNDLE_CLASSPATH);
				}
			}
		}
	}

	private void _replacePropertyValue(
		Properties properties, String key, List<String> values) {

		if (values.isEmpty()) {
			properties.remove(key);
		}
		else {
			properties.setProperty(key, StringUtil.join(values, ","));
		}
	}

	private static final String _COMPILE_INCLUDE_CONFIGURATION =
		"compileInclude";

	private static final Pattern _endPattern = Pattern.compile(
		"-\\w*\\[0-9\\]\\*\\.jar");

}
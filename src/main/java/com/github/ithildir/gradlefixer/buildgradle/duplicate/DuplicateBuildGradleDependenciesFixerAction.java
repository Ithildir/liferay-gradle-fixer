/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.github.ithildir.gradlefixer.buildgradle.duplicate;

import aQute.bnd.osgi.Constants;
import aQute.bnd.version.Version;

import com.github.ithildir.gradlefixer.buildgradle.BaseBuildGradleDependenciesFixerAction;
import com.github.ithildir.gradlefixer.util.BuildGradleUtil;
import com.github.ithildir.gradlefixer.util.DependencyBag;
import com.github.ithildir.gradlefixer.util.FileUtil;
import com.github.ithildir.gradlefixer.util.ModuleDependencyBag;
import com.github.ithildir.gradlefixer.util.ProjectDependencyBag;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Andrea Di Giorgi
 */
public class DuplicateBuildGradleDependenciesFixerAction
	extends BaseBuildGradleDependenciesFixerAction {

	public static final DuplicateBuildGradleDependenciesFixerAction INSTANCE =
		new DuplicateBuildGradleDependenciesFixerAction();

	@Override
	protected void fix(
			List<String> dependencies, Path moduleDirPath, Path rootDirPath)
		throws IOException {

		Map<String, Version> dependencyNameHighestVersions = new HashMap<>();
		Map<String, DependencyBag> dependencyBags = new HashMap<>();

		for (String dependency : dependencies) {
			DependencyBag dependencyBag;

			try {
				dependencyBag = BuildGradleUtil.getDependencyBag(dependency);
			}
			catch (IllegalArgumentException iae) {
				System.err.println(iae.getMessage());

				continue;
			}

			dependencyBags.put(dependency, dependencyBag);

			Object[] nameAndVersion = _getNameAndVersion(
				dependencyBag, rootDirPath);

			String name = (String)nameAndVersion[0];
			Version version = (Version)nameAndVersion[1];

			if ((name == null) || (version == null)) {
				continue;
			}

			Version highestVersion = dependencyNameHighestVersions.get(name);

			if ((highestVersion == null) ||
				(version.compareTo(highestVersion) > 0)) {

				dependencyNameHighestVersions.put(name, version);
			}
		}

		Iterator<String> iterator = dependencies.iterator();

		while (iterator.hasNext()) {
			String dependency = iterator.next();

			DependencyBag dependencyBag = dependencyBags.get(dependency);

			if (dependencyBag == null) {
				continue;
			}

			Object[] nameAndVersion = _getNameAndVersion(
				dependencyBag, rootDirPath);

			String name = (String)nameAndVersion[0];
			Version version = (Version)nameAndVersion[1];

			if ((name == null) || (version == null)) {
				continue;
			}

			Version highestVersion = dependencyNameHighestVersions.get(name);

			if ((version != null) && !highestVersion.equals(version)) {
				iterator.remove();
			}
		}
	}

	private DuplicateBuildGradleDependenciesFixerAction() {
	}

	private Object[] _getNameAndVersion(
			DependencyBag dependencyBag, Path rootDirPath)
		throws IOException {

		String name = null;
		Version version = null;

		if (dependencyBag instanceof ModuleDependencyBag) {
			ModuleDependencyBag moduleDependencyBag =
				(ModuleDependencyBag)dependencyBag;

			name = moduleDependencyBag.getName();
			version = _getVersion(moduleDependencyBag);
		}
		else if (dependencyBag instanceof ProjectDependencyBag) {
			ProjectDependencyBag projectDependencyBag =
				(ProjectDependencyBag)dependencyBag;

			Path projectDirPath = projectDependencyBag.getProjectDirPath(
				rootDirPath);

			Path projectBndPropertiesPath = projectDirPath.resolve("bnd.bnd");

			if (Files.exists(projectBndPropertiesPath)) {
				Properties projectBndProperties = FileUtil.readProperties(
					projectBndPropertiesPath);

				name = projectBndProperties.getProperty(
					Constants.BUNDLE_SYMBOLICNAME);
				version = Version.parseVersion(
					projectBndProperties.getProperty(Constants.BUNDLE_VERSION));
			}
		}
		else {
			System.err.println(
				"Unable to get name and version from " + dependencyBag);
		}

		return new Object[] {name, version};
	}

	private Version _getVersion(ModuleDependencyBag moduleDependencyBag) {
		String version = moduleDependencyBag.getVersion();

		if (version.equalsIgnoreCase("default")) {
			return Version.HIGHEST;
		}

		if (!Version.isVersion(version)) {
			System.err.println("Version " + version + " is not valid");

			return null;
		}

		return Version.parseVersion(version);
	}

}